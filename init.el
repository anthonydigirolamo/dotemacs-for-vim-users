(setq inhibit-startup-message t)
;; Start Maximized
;; (add-to-list 'default-frame-alist '(fullscreen . maximized))

;; Keep track of loading time
(defconst emacs-start-time (current-time))

;; Don't garbage collect durring init
(let ((gc-cons-threshold most-positive-fixnum))

(defun my-minibuffer-setup-hook ()
  (setq gc-cons-threshold most-positive-fixnum))

(defun my-minibuffer-exit-hook ()
  (setq gc-cons-threshold 800000))

(add-hook 'minibuffer-setup-hook #'my-minibuffer-setup-hook)
(add-hook 'minibuffer-exit-hook #'my-minibuffer-exit-hook)

;; keep customize settings in their own file
(setq custom-file "~/.emacs.d/custom.el")
(when (file-exists-p custom-file)
  (load custom-file 'noerror))

;; Initialize all ELPA packages
(require 'package)
(add-to-list 'package-archives '("melpa"        . "http://melpa.org/packages/"))
(add-to-list 'package-archives '("melpa-stable" . "http://stable.melpa.org/packages/" ) t)
(setq package-enable-at-startup nil)
(package-initialize)

;; (benchmark-init/activate)

(let ((elapsed (float-time (time-subtract (current-time) emacs-start-time))))
  (message "Loaded packages in %.3fs" elapsed))

;; Load use-package, used for loading packages
(require 'use-package)

;; Load settings files
(load (expand-file-name "~/.emacs.d/settings-basic.el"))
(load (expand-file-name "~/.emacs.d/settings-packages.el"))
(evil-mode 1)

;; Message how long it took to load everything (minus packages)
(let ((elapsed (float-time (time-subtract (current-time)
                                          emacs-start-time))))
  (message "Loading settings...done (%.3fs)" elapsed))
)  ;; end gc-cons-threshold let

(put 'narrow-to-region 'disabled nil)
(put 'erase-buffer 'disabled nil)
