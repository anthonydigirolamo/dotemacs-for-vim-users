(use-package general
  :ensure t
  ;; not in melpa-stable
)


(use-package saveplace
  ;; builtin package - save last position in a file
  :config
  (when (eq emacs-major-version 24)
    (setq-default save-place t))
  (when (eq emacs-major-version 25)
    (save-place-mode))
  (defadvice find-file (after advice-for-find-file activate) (recenter)) ;; recenter when opening a file?
)


(use-package savehist
  ;; builtin package - save minibuffer history
  :init
  (setq savehist-autosave-interval 150)
  :config
  (savehist-mode))


(use-package s
  :ensure t
  :pin melpa-stable)


(use-package dash
  :ensure t)


(use-package which-key
  :ensure t
  :pin melpa-stable
  :diminish ""
  :init
  ;; (setq which-key-idle-delay 0.5)
  ;; (setq which-key-echo-keystrokes 0)
  ;; (setq echo-keystrokes 0)
  (setq which-key-popup-type 'side-window
        which-key-side-window-location 'bottom
        which-key-show-prefix 'echo)
  :config
  (which-key-mode 1))


(use-package evil
  :ensure t
  :pin melpa-stable
  :demand  ;; load package immediately on startup
  :general
  (:states '(motion)
   ;; hlne movement
   "j" 'evil-next-visual-line
   "k" 'evil-previous-visual-line
   ;; "/" 'swiper
   "C-/" 'swiper
   ;; search using isearch
   ;; "/" 'evil-search-forward
   ;; "k" 'evil-search-next
   ;; "K" 'evil-search-previous
   ;; search using evil's search module
   ;; "/" 'evil-ex-search-forward
   "n" 'evil-ex-search-next
   "N" 'evil-ex-search-previous
   ;; swap * and # to backward and forward respectively
   "*"   'evil-ex-search-word-backward
   "#"   'evil-ex-search-word-forward
   "g *" 'evil-ex-search-unbounded-word-backward
   "g #" 'evil-ex-search-unbounded-word-forward)
  (:states '(normal)
   "C-s" 'save-buffer
   "C-p" 'counsel-git
   "g j" 'amd/join-to-end-of-next-line
   "g s" 'count-words
   "g W" 'macro-join-inner-paragraph
   "C-l" (lambda() (interactive) (evil-ex-nohighlight) (redraw-display))
   "C-k" 'move-line-up
   "C-j" 'move-line-down)
  (:states '(visual)
   "C-k" 'evil-move-lines-up
   "C-j" 'evil-move-lines-down)
  (:states '(insert)
   "C-s" (lambda() (interactive) (save-buffer) (evil-normal-state))
   "C-y" 'counsel-yank-pop
   "M-t" 'ivy-switch-buffer
   ;; "C-w c" 'evil-window-delete
   ;; "C-w o" 'delete-other-windows
   ;; amd/leader-key-insert 'hydra-leader-menu/body
  )
  (:states '(motion visual)
   "RET" 'evil-ex ;; Enter opens : prompt
   ";" 'evil-ex)  ;; semicolon also enters : prompt
  (:states '(motion visual emacs)
   "M-t" 'ivy-switch-buffer
   "M-b" 'ibuffer
   "M-d" (lambda() (interactive) (dired-other-window (projectile-project-root)))
   amd/leader-key 'hydra-leader-menu/body)
  (:states '(motion emacs)
   "C-w J" 'evil-window-move-very-bottom
   "C-w K" 'evil-window-move-very-top
   "C-w H" 'evil-window-move-far-left
   "C-w L" 'evil-window-move-far-right
   "C-w c" 'evil-window-delete
   "C-w o" 'delete-other-windows
   "C-w u" 'winner-undo
   "C-w d" 'winner-redo)
  (:states '(motion) :keymaps 'compilation-mode-map
   "gf" 'find-file-at-point)
  :init
  (setq x-select-enable-clipboard t)
  (setq x-select-enable-clipboard-manager nil)

  ;; Make mouse wheel use evil mode C-d and C-u
  (setq mwheel-scroll-down-function 'evil-scroll-down
        mwheel-scroll-up-function 'evil-scroll-up)

  (setq evil-auto-balance-windows nil)     ;; dont rebalance windows when deleting
  (setq evil-flash-delay .5)
  (setq evil-want-fine-undo 'no)           ;; Make sure undos are done atomically
  (setq evil-want-C-i-jump 'yes)
  (setq evil-want-C-u-scroll 'yes)         ;; find some other way to use emacs C-u?
  (setq evil-want-C-w-in-emacs-state 'yes)
  (setq evil-move-cursor-back nil)         ;; don't move back one charachter when exiting insert

  (setq evil-search-module 'evil-search)   ;; need to set this before loading evil and evil-visualstar
  (setq-default evil-symbol-word-search t) ;; make * and # use the whole word

  ;; join inner paragraph macro
  (fset 'macro-join-inner-paragraph "vipJ^")
  (fset 'key-colon-to-as
    (lambda (&optional arg) "Keyboard macro." (interactive "p") (kmacro-exec-ring-item (quote ([94 100 102 58 36 65 32 97 115 32 escape 112 65 127 44 escape] 0 "%d")) arg)))

  :config
  (evil-mode 1)  ;; enable evil mode
  ;; put the current line at the end of the next line
  (defun amd/join-to-end-of-next-line ()
    (interactive)
    (move-line-down) (join-line))

  ;; Line Bubble Functions
  (defun move-line-up ()
    "move the current line up one line"
    (interactive)
    (transpose-lines 1)
    (previous-line 2))

  (defun move-line-down ()
    "move the current line down one line"
    (interactive)
    (next-line 1)
    (transpose-lines 1)
    (previous-line 1))

  (defun evil-move-lines-up (beg end)
    "Move selected lines up one line."
    (interactive "r")
    (evil-move-lines beg end t))

  (defun evil-move-lines-down (beg end)
    "Move selected lines down one line."
    (interactive "r")
    (evil-move-lines beg end))

  (defun evil-move-lines (beg end &optional move-up)
    "Move selected lines up or down."
    (let ((text (delete-and-extract-region beg end)))
      (if move-up
          (beginning-of-line 0)  ;; move to the beginning of the previous line
          (beginning-of-line 2)) ;; move to the beginning of the next line
      (insert text)
      (forward-char -1)
      (evil-visual-line (- (point) (string-width text)) (point))
    )
  )

  (defun align-no-repeat (start end regexp)
    "Alignment with respect to the given regular expression."
    (interactive "r\nsAlign regexp: ")
    (align-regexp start end
                  (concat "\\(\\s-*\\)" regexp) 1 1 nil))

  (defun align-repeat (start end regexp)
    "Repeat alignment with respect to the given regular expression."
    (interactive "r\nsAlign regexp: ")
    (align-regexp start end
                  (concat "\\(\\s-*\\)" regexp) 1 1 t))

  (defun align-to-space (begin end)
    "Align region to spaces"
    (interactive "r")
    (align-regexp begin end
                  (rx (group (one-or-more (syntax whitespace))) ) 1 1 t)
    (evil-indent begin end))

  (defun align-to-comma (begin end)
    "Align region to comma signs"
    (interactive "r")
    (align-regexp begin end
                  (rx "," (group (zero-or-more (syntax whitespace))) ) 1 1 t))

  (defun align-to-colon (begin end)
    "Align region to colon"
    (interactive "r")
    (align-regexp begin end
                  (rx ":" (group (zero-or-more (syntax whitespace))) ) 1 1 ))

  (defun align-to-equals (begin end)
    "Align region to equal signs"
    (interactive "r")
    (align-regexp begin end
                  (rx (group (zero-or-more (syntax whitespace))) "=") 1 1 ))

  (defun align-interactively ()
    "invoke align-regexp interactively"
    (interactive)
    (let ((current-prefix-arg 4)) ;; emulate C-u
      (call-interactively 'align-regexp)))

  ;; ;; ESC changes
  ;; ;; NOTE: ESC is Meta inside a terminal
  ;; (global-unset-key (kbd "ESC ESC ESC"))
  ;; (global-unset-key (kbd "ESC ESC"))
  ;; (defun amd/minibuffer-keyboard-quit ()
  ;;   "Abort recursive edit. In Delete Selection mode, if the mark is active, just deactivate it; then it takes a second \\[keyboard-quit] to abort the minibuffer."
  ;;   (interactive)
  ;;   (if (and delete-selection-mode transient-mark-mode mark-active)
  ;;       (setq deactivate-mark  t)
  ;;     (when (get-buffer "*Completions*") (delete-windows-on "*Completions*"))
  ;;     (abort-recursive-edit)))

  ;; (define-key evil-normal-state-map           [escape] 'keyboard-quit)
  ;; (define-key evil-visual-state-map           [escape] 'keyboard-quit)
  ;; (define-key evil-emacs-state-map            [escape] 'keyboard-quit)
  ;; (define-key minibuffer-local-map            [escape] 'amd/minibuffer-keyboard-quit)
  ;; (define-key minibuffer-local-ns-map         [escape] 'amd/minibuffer-keyboard-quit)
  ;; (define-key minibuffer-local-completion-map [escape] 'amd/minibuffer-keyboard-quit)
  ;; (define-key minibuffer-local-must-match-map [escape] 'amd/minibuffer-keyboard-quit)
  ;; (define-key minibuffer-local-isearch-map    [escape] 'amd/minibuffer-keyboard-quit)

  ;; Center Screen on search hit
  ;;   (not sure I want these)
  ;; (advice-add 'evil-ex-search-word-forward            :after #'recenter)
  ;; (advice-add 'evil-ex-search-word-backward           :after #'recenter)
  ;; (advice-add 'evil-ex-search-unbounded-word-forward  :after #'recenter)
  ;; (advice-add 'evil-ex-search-unbounded-word-backward :after #'recenter)
  ;; (advice-add 'evil-ex-search-next                    :after #'recenter)
  ;; (advice-add 'evil-ex-search-previous                :after #'recenter)

  ;; (defadvice evil-ex-search-next (after advice-for-evil-ex-search-next activate)
  ;;   (evil-scroll-line-to-center (line-number-at-pos)))
  ;; (defadvice evil-ex-search-previous (after advice-for-evil-ex-search-previous activate)
  ;;   (evil-scroll-line-to-center (line-number-at-pos)))

  (advice-add 'evil-jump-forward  :after #'recenter)
  (advice-add 'evil-jump-backward :after #'recenter)

  (add-to-list 'evil-emacs-state-modes 'dired-mode)
  (add-to-list 'evil-emacs-state-modes 'makey-key-mode)
  (add-to-list 'evil-emacs-state-modes 'magit-popup-mode)
  (add-to-list 'evil-normal-state-modes 'git-commit-mode)

  (add-to-list 'evil-motion-state-modes 'package-menu-mode)
  (add-to-list 'evil-motion-state-modes 'paradox-menu-mode)
  (add-to-list 'evil-motion-state-modes 'flycheck-error-list-mode)
)


(use-package smex  ;; used by counsel
  :ensure t
  :pin melpa-stable
)


(use-package ivy
  :ensure t
  :pin melpa-stable
  :demand  ;; load package immediately on startup
  :init
  (setq ivy-display-style 'fancy)
  (setq ivy-use-virtual-buffers t)
  (setq ivy-height 12)
  (setq enable-recursive-minibuffers t)
  ;; Use the overlay for everything
  ;; (setq ivy-display-function 'ivy-display-function-overlay)
  :config
  (ivy-mode 1)
)


(use-package hydra
  :ensure t
  :pin melpa-stable
  :config
  (defhydra hydra-leader-menu (:color blue :hint nil)
    ("d"  counsel-find-file  "files find-file" :column "File/Buffer")
    ("ft" neotree            "files neotree")
    ("fr" ivy-recentf        "files recentf")
    ("bb" ivy-switch-buffer  "buffer switch")
    ("bi" ibuffer            "buffer ibuffer")
    ("bk" kill-this-buffer   "buffer kill")
    ("br" revert-buffer      "buffer revert")
    ("j"  counsel-imenu      "jump imenu")

    ("la" counsel-linux-app                "linux apps" :column "Menus/Help")
    ("lt" load-theme                       "load theme")
    ("lc" list-colors-display              "list colors")
    ("lf" list-faces-display               "list faces")
    ("lp" package-list-packages            "list packages")
    ("hk" counsel-descbinds                "help keys")
    ("hK" which-key-show-top-level         "help whichkey")
    ("hv" helpful-variable                 "help variable")
    ("hf" helpful-function                 "help function")
    ("hm" (describe-variable 'major-mode)  "help mode")
    ("bu" browse-url-generic               "browse url")

    ("ar" align-repeat           "align repeat" :column "Align")
    ("an" align-no-repeat        "align no-repeat")
    ("a:" align-to-colon         "align :")
    ("a=" align-to-equals        "align =")
    ("a," align-to-comma         "align ,")
    ("as" align-to-space         "align whitespace")
    ("ai" align-interactively    "align interactive")
    ("G"  counsel-git-grep       "search git grep")
    ("pt" counsel-pt             "search pt")
    ("/"  counsel-grep-or-swiper "search grep/swiper")

    ("g"  magit-status                     "git" :column "Git")
    ("rc" compile                          "run compile")
    ("rr" recompile                        "run recompile")
    ("u" undo-tree-visualize               "undo-tree")
    ("v" (find-file user-emacs-directory)  "open .emacs")
    ("c" calc-dispatch                     "calc")

    ("zi" (text-scale-increase 0.5)  "zoom-in" :color pink :column "Misc")
    ("zo" (text-scale-decrease 0.5)  "zoom-out" :color pink)
    ("DS" desktop-save               "desktop-save")
    ("DC" desktop-clear              "desktop-clear")
    ("DL" desktop-read               "desktop-read")
    ("sd" server-edit                "server done")
    ("st" toggle-truncate-lines      "set truncate-lines")
    ("sf" auto-fill-mode             "set auto-fill-mode")
    ("WC" count-words                "word-count")
    ("x" counsel-M-x                 "M-x")
    ("q"  keyboard-quit              "close")
  )
)


(use-package ivy-hydra
  :ensure t
  :pin melpa-stable
)


(use-package swiper
  :ensure t
  :pin melpa-stable
  :diminish ivy-mode
  :general
  (:keymaps '(ivy-minibuffer-map)
    "<C-return>" 'ivy-alt-done
    "<C-M-return>" 'ivy-immediate-done)
  (:states '(normal)
   :keymaps '(ivy-occur-grep-mode-map)
    "j" 'ivy-occur-next-line
    "k" 'ivy-occur-previous-line
    "C-j" 'next-error-no-select
    "C-k" 'previous-error-no-select)

  :config
  (eval-after-load "ivy"
    `(progn
       (define-key ivy-minibuffer-map (kbd "<escape>") 'minibuffer-keyboard-quit)))

  (eval-after-load "ivy-hydra"
    `(progn
       (define-key hydra-ivy/keymap (kbd "h") 'hydra-ivy/ivy-prev-action)
       (define-key hydra-ivy/keymap (kbd "l") 'hydra-ivy/ivy-next-action)
       (define-key hydra-ivy/keymap (kbd "j") 'hydra-ivy/ivy-next-line)
       (define-key hydra-ivy/keymap (kbd "k") 'hydra-ivy/ivy-previous-line)))

  (defun amd/update-evil-search ()
    "Update evil search pattern with swiper regex and recenter."
    (recenter)
    (let ((count 1)
          (direction 'forward)
          (regex (ivy--regex ivy-text)))
      ;; This bit is mostly taken from evil-ex-start-word-search
      (setq evil-ex-search-count count
            evil-ex-search-direction direction
            evil-ex-search-pattern (evil-ex-make-search-pattern regex)
            evil-ex-search-offset nil
            evil-ex-last-was-search t)
      ;; update search history unless this pattern equals the previous pattern
      (unless (equal (car-safe evil-ex-search-history) regex)
        (push regex evil-ex-search-history))
      (evil-push-search-history regex (eq direction 'forward))
      ;; set the highlight
      (evil-ex-search-activate-highlight evil-ex-search-pattern)))

  (advice-add 'swiper                 :after #'amd/update-evil-search)
  (advice-add 'counsel-grep           :after #'amd/update-evil-search)
  (advice-add 'counsel-grep-or-swiper :after #'amd/update-evil-search)
)


(use-package counsel
  :ensure t
  :pin melpa-stable
  :after smex
  :bind (("M-x"     . counsel-M-x)
         ("C-h k" . counsel-descbinds)
         ("C-h C-k" . counsel-descbinds))
  :init
  (setq counsel-grep-base-command "grep -niE \"%s\" %s")
)


(use-package helpful
  :ensure t
  :pin melpa-stable
  :after (counsel evil)
  :bind (("C-h v"   . helpful-variable)
         ("C-h C-v" . helpful-variable)
         ("C-h f"   . helpful-function)
         ("C-h C-f" . helpful-function))
  :init
  (add-to-list 'evil-motion-state-modes 'helpful-mode)
  (add-to-list 'evil-motion-state-modes 'elisp-refs-mode)
  :config
  (ivy-set-actions
   'counsel-M-x
   '(("d" counsel--find-symbol "definition")
     ("h" (lambda (x) (helpful-function (intern x))) "helpful")))
)


(use-package undo-tree
  :ensure t
  :diminish ""
  :init
  (setq undo-tree-visualizer-timestamps t
        undo-tree-visualizer-diff t)
  :general
  (:states '(motion) :keymaps '(undo-tree-visualizer-mode-map)
    "j" 'undo-tree-visualize-redo
    "k" 'undo-tree-visualize-undo
    "h" 'undo-tree-visualize-switch-branch-left
    "l" 'undo-tree-visualize-switch-branch-right)
)


(use-package ibuffer
  ;; builtin package - shows current buffers in a list
  :init
  (add-hook 'ibuffer-mode-hook (lambda () (hl-line-mode 1)))
  (setq ibuffer-use-header-line t
        ibuffer-truncate-lines t
        ibuffer-use-other-window t)
  ;; set column widths
  (setq ibuffer-formats
        '((mark modified read-only locked " "
                (name 32 32 :left :elide)
                " "
                (size 9 -1 :right)
                " "
                (mode 16 16 :left :elide)
                " " filename-and-process)
          (mark " "
                (name 16 -1)
                " " filename)))
  :general
  (:states '(emacs)
   :keymaps 'ibuffer-mode-map
   ;; "RET" 'ibuffer-visit-buffer-other-window ;; default is "o"
   ;; "a" 'ibuffer-visit-buffer                ;; default is "RET"
   "j" 'ibuffer-forward-line
   "k" 'ibuffer-backward-line
   "/" 'swiper
   "C-/" 'swiper
  )
)

;; group ibuffers by version controlled directories
(use-package ibuffer-vc
  :ensure t
  :pin melpa-stable
  :init
  (add-hook 'ibuffer-mode-hook (lambda () (ibuffer-auto-mode 1)))
  (add-hook 'ibuffer-hook
    (lambda ()
      (ibuffer-vc-set-filter-groups-by-vc-root)
      (unless (eq ibuffer-sorting-mode 'alphabetic)
        (ibuffer-do-sort-by-alphabetic))))
)


(use-package dired
  ;; builtin package - list files (similar to netrw)
  :defer t
  :init
  (put 'dired-find-alternate-file 'disabled nil)
  ;; Suggest locations for operations, ie midnight commander style copy if another window is open.
  (setq dired-dwim-target t)
  ;; Auto refresh dired, but be quiet about it
  (setq dired-auto-revert-buffer t)
  ;; (setq global-auto-revert-non-file-buffers t)
  (setq auto-revert-verbose nil)
  ;; Always copy/delete recursively
  (setq dired-recursive-copies  'always
        dired-recursive-deletes 'top)
  (setq wdired-allow-to-change-permissions t)
  (setq insert-directory-program
        (or (executable-find "gls")
            (executable-find "ls")))

  :general
  (:states '(emacs)
   :keymaps 'dired-mode-map
   ;; "RET" 'dired-find-file ;; default
   ;; "f" 'dired-find-file ;; default
   ;; "a" 'dired-find-alternate-file ;; default - open a file in the same buffer and close dired
   "C-p" 'counsel-git
   "C-l" (lambda() (interactive) (revert-buffer) (evil-ex-nohighlight) (recenter))
   "gr" 'revert-buffer
   "gg" 'evil-goto-first-line
   "G" 'evil-goto-line
   "/" 'swiper
   "C-/" 'swiper
   "C-c C-w" 'dired-toggle-read-only
   ;; default dired-writable mode is C-x C-q
   ;;   press C-c C-c to commit
   "h" 'left-char
   "l" 'right-char
   "-" 'dired-hide-details-mode
   "j" 'dired-next-line
   "k" 'dired-previous-line
   "J" 'dired-next-dirline
   "K" 'dired-prev-dirline)
  :config
  ;; hide ls -l style output - can be toggled by hitting (
  (add-hook 'dired-mode-hook 'dired-hide-details-mode)

  (defadvice dired-toggle-read-only (after advice-for-dired-toggle-read-only activate)
    (evil-normal-state))
)


(use-package magit
  :ensure t
  :pin melpa-stable
  :defer t
  :init
  (setq magit-last-seen-setup-instructions "1.4.0")
  (setq magit-diff-expansion-threshold 10.0)

  :general
  (:keymaps '(magit-popup-mode-map)
   amd/leader-key 'amd/quit-magit-and-leader)
  (:keymaps '(magit-log-mode-map
              magit-diff-mode-map
              magit-process-mode-map
              magit-status-mode-map)
   "C-p" 'counsel-git
   "e" 'magit-section-backward
   "p" nil) ;; hit E for ediff popup instead

  :config
  (evil-define-minor-mode-key 'emacs 'magit-popup-mode [escape] 'magit-popup-quit)

  (defun amd/quit-magit-and-leader ()
    "Quit Magit Popup and display leader menu."
    (interactive)
    (magit-popup-quit)
    (hydra-leader-menu/body))
)


(use-package neotree
  :ensure t
  :pin melpa-stable
  :after evil
  :commands (neotree)
  :init
  (setq neo-smart-open t)
  (add-to-list 'evil-emacs-state-modes 'neotree-mode)
)


(use-package doom-modeline
  :ensure t
  :pin melpa-stable
  :hook (after-init . doom-modeline-mode)
  ;; for nice GUI icons must run: M-x all-the-icons-install-fonts
)


(use-package doom-themes
  :ensure t
  :pin melpa-stable
  :init
  (setq doom-themes-enable-bold t    ; if nil, bold is universally disabled
        doom-themes-enable-italic t) ; if nil, italics is universally disabled
  (setq doom-neotree-enable-folder-icons t)
  (setq doom-neotree-enable-file-icons 'simple)
  :config
  (load-theme 'doom-molokai t)
  (doom-themes-org-config)
  (doom-themes-neotree-config)
)


(use-package company
  :ensure t
  :pin melpa-stable
  :demand
  :diminish ""
  :init
  (setq company-idle-delay 0.2)
  (setq company-minimum-prefix-length 1)
  (setq company-show-numbers t)
  (setq company-tooltip-limit 20)
  (setq company-dabbrev-downcase nil)
  (setq company-dabbrev-ignore-case nil)
  :general
  (:states '(insert)
    "C-x C-f" 'company-files)
  :config
  (global-company-mode t)
  (setq company-backends
        '((company-files
           company-keywords
           company-capf
           company-yasnippet)
          (company-abbrev company-dabbrev)))
  ;; Abort company-mode popup when exiting insert mode
  (defun abort-company-on-insert-state-exit ()
    (company-abort))
  (add-hook 'evil-insert-state-exit-hook 'abort-company-on-insert-state-exit)
)
