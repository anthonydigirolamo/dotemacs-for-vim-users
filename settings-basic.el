;; Leader key
(setq amd/leader-key ",")
(setq amd/leader-key-insert "C-,")

;; General emacs options
(require 'mouse)
(xterm-mouse-mode t)

(setq inhibit-startup-screen t)
(setq echo-keystrokes 0.2)

(setq ring-bell-function (lambda ()))
(setq recenter-redisplay nil) ;; don't redraw the whole display when recentering

(setq-default fill-column 80)
;; (add-hook 'text-mode-hook 'turn-on-auto-fill) ;; get auto line breaks at fill-column - auto-fill-mode
(set-display-table-slot standard-display-table 'wrap ?\ ) ;; Hide the \ at the end of each wrapped line. Don't really need it with relative-line-numbers
;; toggle-truncate-lines will toggle line wrapping
;; auto-fill-mode will insert line breaks automatically

;; Hide GUI elements
(when (fboundp 'tool-bar-mode) (tool-bar-mode -1))
(when (fboundp 'scroll-bar-mode) (scroll-bar-mode -1))
(menu-bar-mode -1)

;; Save Tempfiles in a temp dir
(setq backup-directory-alist `((".*" . ,temporary-file-directory)))
(setq auto-save-file-name-transforms `((".*" ,temporary-file-directory t)))

;; Stop making backup files
(setq make-backup-files nil)

(defalias 'yes-or-no-p 'y-or-n-p) ;; no more typing out y.e.s.

;; (set-default 'show-trailing-whitespace t)
(setq delete-trailing-lines nil)
(add-hook 'before-save-hook 'delete-trailing-whitespace) ;; Erase trailing whitespace before save

;; Indentation
(setq-default c-basic-indent 2)
(setq-default tab-width 2)          ;; set tw=2
(setq-default indent-tabs-mode nil) ;; set expandtab

;; Scroll just one line when hitting bottom of window
(setq scroll-conservatively 10000)
;; (setq scroll-step 1)

(setq mouse-wheel-scroll-amount '(1 ((shift) . 1)) ;; one line at a time
      mouse-wheel-progressive-speed nil            ;; don't accelerate scrolling
      mouse-wheel-follow-mouse 't)                 ;; scroll window under mouse

(setq mouse-wheel-down-event 'mouse-5) ;; Swap up/down scroll direction
(setq mouse-wheel-up-event 'mouse-4)
;; (setq mouse-wheel-flip-direction nil) ;; Swap left/right scroll direction?
(setq auto-window-vscroll nil) ;; cursor movement lag reduction?
;; Mac OSX Emacs Settings
(setq ns-alternate-modifier 'meta
      ns-command-modifier 'meta)


;; Set the right font name and size by OS.
(cond
 ((eq system-type 'gnu/linux)
  (setq amd/font-size 16
        amd/font-name "Iosevka SS08-%d:antialias=true:hinting=true:autohint=false:hintstyle=hintslight"))
 (t
  (setq amd/font-size 16
        amd/font-name "Iosevka SS08-%d")))

(defun amd/set-font ()
  "Set preferred GUI font."
  (interactive)
  (set-frame-font (format amd/font-name amd/font-size))
)

(amd/set-font)
