# A ~/.emacs.d for vim users

## Getting Started

If you have `.emacs` or `.emacs.d` in your homedir move or delete them.

    git clone https://gitlab.com/anthonydigirolamo/dotemacs-for-vim-users.git ~/.emacs.d/
